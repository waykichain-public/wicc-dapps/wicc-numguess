const dataFormate = (date) => {
    let d = new Date(date)
    let year = d.getFullYear()
    let month = d.getMonth() + 1
    let day = d.getDate()
    return `${year}-${month}-${day}`
} 

export default {
    dataFormate
}