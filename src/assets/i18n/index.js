import Vue from 'vue'
import VueI18n from 'vue-i18n'
import en from './en'
import zh from './zh'

Vue.use(VueI18n)

// 以下为语言包单独设置的场景，单独设置时语言包需单独引入
const messages = {
  'zh':zh, //中文繁体语言包
  'en': en  // 英文语言包
}
let lang= navigator.language.substr(0,2)==='zh'?'zh':'en';
export default new VueI18n({
  locale: lang, // set locale 默认显示英文
  messages: messages // set locale messages
});