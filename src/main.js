// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router/Index.js'
import "@/assets/js/rem.js"
import "@/assets/css/public.scss"
import "babel-polyfill";
import {
  ToastPlugin,
  LoadingPlugin
} from 'vux'
import axios from 'axios'
import VConsole from 'vconsole'
import i18n from '@/assets/i18n/index.js'
// Vue.prototype.$http = axios
Vue.use(LoadingPlugin)
Vue.use(ToastPlugin)
// axios.defaults.headers.common['Cache-Control'] = 'no-cache';
// const instance = axios.create({
//   headers: {
//     "Cache-Control": "no-cache"
//   }
// });

Vue.prototype.$http = axios;
// var vConsole = new VConsole();
// if (process.env.NODE_ENV !== 'production') {
// var vConsole = new VConsole();
// }
Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  i18n,
  components: {
    App
  },
  template: '<App/>'
})
